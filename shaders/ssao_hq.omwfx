sampler_2d noise {
    source = "shaders/textures/poisson_nrm.dds";
    mag_filter = nearest;
    min_filter = nearest;
    wrap_s = repeat;
    wrap_t = repeat;
}

uniform_bool save_my_hands {
    default = true;
    display_name = "Mask hands";
    description = "Hack to hide hands, there might be other casualties of war...";
}

uniform_float max_ray_radius {
    default = 8.0;
    min = 8.0;
    max = 100.0;
    step = 1.0;
    description = "Max ray radius, world units.";
}

uniform_float multiplier {
    default = 1.8;
    min = 0.1;
    max = 10.0;
    step = 0.1;
    description = "Overall strength. 1.0 is the correct physical value.";
}

uniform_float occlusion_falloff {
    default = 7.0;
    min = 0.0;
    max = 100.0;
    step = 1.0;
    description = "Occlusion falloff. More means less depth precision, and more strength.";
}

uniform_float blur_falloff {
    default = 0.02;
    min = 0.0;
    max = 1.0;
    step = 0.01;
    description = "Blur depth falloff, more means a larger depth range is blurred.";
}

uniform_float blur_radius {
    default = 8.0;
    min = 0.0;
    max = 100.0;
    step = 1.0;
    description = "Blur radius in pixels";
}

shared {

    const float sky = 1e6;
    #define N 6 // Samples: 48, 32, 16, 10, 6. More means precision and performance hit.

    vec2 rcpres = omw.rcpResolution;

    vec2 t = 2.0 * tan(radians(omw.fov * 0.5)) * vec2(1.0, rcpres.x / rcpres.y);

    float getLinearDepth(in vec2 tex)
    {
        float d = omw_GetDepth(tex);
        float ndc = d * 2.0 - 1.0;

        return omw.near * omw.far / (omw.far + ndc * (omw.near - omw.far));
    }

    vec2 fromView(vec3 view)
    {
        return vec2(view.xy / t / view.z + 0.5);
    }

    vec3 toView(vec2 tex)
    {
        float depth = min(getLinearDepth(tex), sky);
        vec2 xy = (tex - 0.5) * depth * t;
        return vec3(xy, depth);
    }
}

fragment ssao {
    omw_In vec2 omw_TexCoord;

    vec2 pack2(float f)
    {
        return vec2(f, fract(f * 255.0 - 0.5));
    }

    vec3 dirs[N] = vec3[]
    (
    #if N >= 48
        vec3(0.04556, 0.09479, -0.28152),
        vec3(0.08758, -0.17459, 0.14837),
        vec3(0.57190, 0.49847, -0.64101),
        vec3(0.03734, 0.08883, 0.02760),
        vec3(0.09542, 0.00860, -0.06100),
        vec3(-0.09026, 0.10302, -0.19118),
        vec3(0.54615, -0.08996, 0.65708),
        vec3(0.10383, -0.02091, -0.00712),

        vec3(-0.18331, -0.10469, -0.10359),
        vec3(0.74090, 0.54747, 0.29408),
        vec3(-0.00270, 0.00443, 0.00937),
        vec3(0.57718, -0.34843, -0.46236),
        vec3(-0.05405, -0.22931, -0.17499),
        vec3(-0.30069, -0.47800, -0.09799),
        vec3(-0.00606, -0.21609, -0.03372),
        vec3(-0.87538, 0.02272, -0.20764),
     #endif
     #if N >= 32
        vec3(-0.48710, 0.29981, -0.25607),
        vec3(0.03163, 0.02985, 0.15875),
        vec3(-0.18035, -0.06732, 0.58081),
        vec3(0.17450, -0.02599, -0.33065),
        vec3(0.06349, -0.03883, 0.03387),
        vec3(0.10394, 0.69917, -0.16286),
        vec3(0.04695, 0.36486, -0.33281),
        vec3(0.31748, -0.27097, -0.03810),

        vec3(-0.07172, 0.09571, -0.00005),
        vec3(-0.06029, 0.38936, 0.07889),
        vec3(0.13681, -0.30447, 0.04381),
        vec3(-0.09649, -0.92119, 0.34473),
        vec3(-0.56596, -0.03144, 0.18844),
        vec3(-0.10715, 0.25182, -0.13061),
        vec3(0.59962, 0.21747, -0.07792),
        vec3(-0.03793, -0.03653, -0.06986),
    #endif
    #if N >= 16
        vec3(-0.00941, -0.00326, -0.05597),
        vec3(0.11686, 0.00831, 0.04915),
        vec3(-0.08125, -0.24638, 0.30141),
        vec3(0.35193, 0.29639, 0.47544),
        vec3(0.32063, -0.70203, -0.40622),
        vec3(-0.37344, -0.18112, 0.37140),
    #endif
    #if N >= 10
        vec3(-0.73605, -0.39320, 0.04992),
        vec3(0.02274, 0.21583, 0.19429),
    #endif
    #if N >= 8
        vec3(0.00762, -0.01247, 0.03311),
        vec3(-0.61057, 0.20510, 0.58876),
    #endif
        vec3(0.55319, 0.67960, -0.19194),
        vec3(-0.43533, 0.62404, 0.45133),
        vec3(-0.02386, -0.03104, 0.01502),
        vec3(-0.20990, 0.10082, 0.03849),
        vec3(0.06331, -0.17620, -0.31359),
        vec3(-0.12261, 0.00720, -0.12465)
    );

    void main()
    {
        const float depth_scale = 10000;

        vec3 pos = toView(omw_TexCoord);
        float xylength = sqrt(1 - omw.eyeVec.z * omw.eyeVec.z);

        float savemyhands = 1.0;

        if (save_my_hands)
        {
            const float unit2m = 0.0142;
            const float k = 0.00001;
            float t =  2.0 * tan(radians(0.5 * omw.fov));
            float s = getLinearDepth(vec2(0.5)) * unit2m;
            float depth = getLinearDepth(omw_TexCoord);
            float z_corr = length(vec3((omw_TexCoord.x - 0.5) * t, (omw_TexCoord.y - 0.5) * t / omw.rcpResolution.y * omw.rcpResolution.x, 1));
            float z = z_corr * unit2m * depth;
            savemyhands = smoothstep(0.568, 0.781, z);
        }

        if(pos.z <= 0 || pos.z > sky)
        {
            omw_FragColor = vec4(0, 0, 0, 1);
            return;
        }

        vec3 left = pos - toView(omw_TexCoord + rcpres * vec2(-1, 0));
        vec3 right = toView(omw_TexCoord + rcpres * vec2(1, 0)) - pos;
        vec3 up = pos - toView(omw_TexCoord + rcpres * vec2(0, -1));
        vec3 down = toView(omw_TexCoord + rcpres * vec2(0, 1)) - pos;

        vec3 dx = length(left) < length(right) ? left : right;
        vec3 dy = length(up) < length(down) ? up : down;

        vec3 normal = normalize(cross(dy, dx));
        dy = normalize(cross(dx, normal));
        dx = normalize(dx);

        vec3 rnd = texture2D(noise, omw_TexCoord / rcpres / 8).xyz * 2 - 1;

        float AO = 0, amount = 0;
        for (int j = 0; j < N; j++)
        {
            vec3 ray, occ;

            ray = reflect(dirs[j] * max_ray_radius, rnd);

            ray *= sign(ray.z);
            ray = dx * ray.x + dy * ray.y + normal * ray.z;
            float weight = dot(normalize(ray), normal);

            occ = toView(fromView(pos + ray));
            // FIXME: depth sampling should be linear
            // float diff = pos.z + ray.z - occ.z;
            // Bias occ.z to avoid self-occlusion errors from point sampling depth
            float diff = pos.z + ray.z - 1.00025 * occ.z;

            amount += weight;
            AO += weight * step(0, diff) * exp2(-diff / occlusion_falloff);
        }

        AO *= savemyhands;

        omw_FragColor = vec4(AO / amount, pack2(pos.z / depth_scale), 1);
    }
}

fragment smartblur {
    omw_In vec2 omw_TexCoord;

    #define M 12
    vec2 taps[M] = vec2[]
    (
        vec2(-0.695914,0.457137), vec2(-0.203345,0.620716),
        vec2(0.96234,-0.194983), vec2(0.473434,-0.480026),
        vec2(0.507431,0.064425), vec2(0.89642,0.412458),
        vec2(-0.32194,-0.932615), vec2(-0.791559,-0.59771),
        vec2(-0.326212,-0.40581), vec2(-0.840144,-0.07358),
        vec2(0.519456,0.767022), vec2(0.185461,-0.893124)
    );

    float unpack2(vec2 f)
    {
        return f.x + ((f.y - 0.5) / 255.0);
    }

    void main()
    {
        vec4 data = omw_GetLastPass(omw_TexCoord); //FIXME: mirror
        float total = data.r;
        float depth = unpack2(data.gb);
        float rev = blur_radius * (2.0*data.a - 1.0);
        float amount = 1;
        for (int i = 0; i < M; i++)
        {
            vec2 s_uv = omw_TexCoord + rcpres * taps[i] * rev;
            vec3 s_data = omw_GetLastPass(s_uv).rgb; //FIXME: mirror
            float s_depth = unpack2(s_data.gb);
            float weight = exp2(-abs(depth - s_depth) / depth / blur_falloff);

            amount += weight;
            total += s_data.r * weight;
        }

        omw_FragColor = vec4(total / amount, data.gb, 1.0 - data.a);
    }
}

fragment combine {
    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 scene = omw_GetLastShader(omw_TexCoord);

        float lum = dot(scene.rgb, vec3(0.2125, 0.7154, 0.0721));
        const float width = 0.3;
        const float sigma = 1.0;
        const float threshold = 0.7; // configurable
        float f = pow((1 - (min(1,max(0, lum - threshold + width))) / (2 * width)), sigma);

		float fogcov =1-omw_EstimateFogCoverageFromUV(omw_TexCoord);

        float final = fogcov * multiplier * omw_GetLastPass(omw_TexCoord).r * f;
        vec3 result = mix(scene.rgb, omw.fogColor.rgb * (1-fogcov), final);

        omw_FragColor = vec4(result, 1.0);
    }
}

technique {
    passes = ssao, smartblur, smartblur, combine;
    description = "SSAO, high quality";
    author = "Knu";
    version = "1.0";
}
