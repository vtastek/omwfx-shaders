## Features

- Screen Space Reflections, Dynamic Puddles(wetworld)
- SSAO HQ improvements
- Motion Blur

## Installation

SSR needs wetworld to work

Shader order must look like:

```
wetworld
SSR
```